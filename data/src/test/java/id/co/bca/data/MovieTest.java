package id.co.bca.data;


import org.junit.Test;

import id.co.bca.data.entity.MoviesRespEntity;
import id.co.bca.data.entity.ReviewsRespEntity;
import id.co.bca.data.entity.TrailersRespEntity;
import id.co.bca.data.net.MoviesService;
import id.co.bca.data.net.ServiceGenerator;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

public class MovieTest {

    @Test
    public void T001_GetPopularMovies(){
        MoviesService moviesService = ServiceGenerator.getMoviesService();
        Single<MoviesRespEntity> resp = moviesService.getPopularMovies();

        TestObserver<MoviesRespEntity> testObserver = new TestObserver<>();
        resp.subscribe(testObserver);
        testObserver.assertComplete();
        testObserver.assertNoErrors();
    }

    @Test
    public void T002_GetTopRatedMovies(){
        MoviesService moviesService = ServiceGenerator.getMoviesService();
        Single<MoviesRespEntity> resp = moviesService.getTopRatedMovies();

        TestObserver<MoviesRespEntity> testObserver = new TestObserver<>();
        resp.subscribe(testObserver);
        testObserver.assertComplete();
        testObserver.assertNoErrors();
    }

    @Test
    public void T003_GetTrailers(){
        MoviesService moviesService = ServiceGenerator.getMoviesService();
        Single<TrailersRespEntity> resp = moviesService.getTrailers("330457");

        TestObserver<TrailersRespEntity> testObserver = new TestObserver<>();
        resp.subscribe(testObserver);
        testObserver.assertComplete();
        testObserver.assertNoErrors();
    }

    @Test
    public void T003_GetReviews(){
        MoviesService moviesService = ServiceGenerator.getMoviesService();
        Single<ReviewsRespEntity> resp = moviesService.getReviews("330457");

        TestObserver<ReviewsRespEntity> testObserver = new TestObserver<>();
        resp.subscribe(testObserver);
        testObserver.assertComplete();
        testObserver.assertNoErrors();
    }
}
