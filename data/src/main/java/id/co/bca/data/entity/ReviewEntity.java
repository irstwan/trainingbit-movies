package id.co.bca.data.entity;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class ReviewEntity {
    @SerializedName("author")
    private String author;
    @SerializedName("id")
    private String id;
    @SerializedName("content")
    private String content;
    @SerializedName("url")
    private String url;
}
