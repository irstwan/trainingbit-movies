package id.co.bca.data.net;

import id.co.bca.data.BuildConfig;
import id.co.bca.data.entity.MoviesRespEntity;
import id.co.bca.data.entity.ReviewsRespEntity;
import id.co.bca.data.entity.TrailersRespEntity;
import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MoviesService {
    @GET("popular?api_key="+ BuildConfig.API_KEY)
    Single<MoviesRespEntity> getPopularMovies();

    @GET("top_rated?api_key="+ BuildConfig.API_KEY)
    Single<MoviesRespEntity> getTopRatedMovies();

    @GET("{movieId}/videos?api_key="+ BuildConfig.API_KEY)
    Single<TrailersRespEntity> getTrailers(@Path("movieId") String movieId);

    @GET("{movieId}/reviews?api_key="+ BuildConfig.API_KEY)
    Single<ReviewsRespEntity> getReviews(@Path("movieId") String movieId);
}
