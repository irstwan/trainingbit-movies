package id.co.bca.data.net;

import id.co.bca.data.BuildConfig;

public class ServiceGenerator {
    private static MoviesService moviesService;

    public static MoviesService getMoviesService(){
        if (moviesService==null){
            moviesService = RetrofitHelper.getRetrofit(BuildConfig.BASE_URL_MOVIE).create(MoviesService.class);
        }
        return moviesService;
    }
}
