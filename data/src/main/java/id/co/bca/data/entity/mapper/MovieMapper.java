package id.co.bca.data.entity.mapper;
import java.util.ArrayList;
import java.util.List;

import id.co.bca.data.entity.MovieEntity;
import id.co.bca.data.entity.MoviesRespEntity;
import id.co.bca.data.entity.ReviewEntity;
import id.co.bca.data.entity.ReviewsRespEntity;
import id.co.bca.data.entity.TrailerEntity;
import id.co.bca.data.entity.TrailersRespEntity;
import id.co.bca.domain.model.Movie;
import id.co.bca.domain.model.MoviesResp;
import id.co.bca.domain.model.Review;
import id.co.bca.domain.model.ReviewsResp;
import id.co.bca.domain.model.Trailer;
import id.co.bca.domain.model.TrailersResp;

public class MovieMapper {

    public MoviesResp moviesRespToDomain(MoviesRespEntity param){
        MoviesResp resp = new MoviesResp();
        List<Movie> listMovie = new ArrayList<>();
        resp.setPage(param.getPage());
        resp.setTotalPages(param.getTotalPages());
        resp.setTotalResults(param.getTotalResults());

        for (int i = 0; i < param.getResults().size(); i++){
            MovieEntity movieEntity = param.getResults().get(i);
            Movie movie = new Movie();
            movie.setAdult(movieEntity.getAdult());
            movie.setBackdropPath(movieEntity.getBackdropPath());
            movie.setGenreIds(movieEntity.getGenreIds());
            movie.setId(movieEntity.getId());
            movie.setOriginalLanguage(movieEntity.getOriginalLanguage());
            movie.setOriginalTitle(movieEntity.getOriginalTitle());
            movie.setOverview(movieEntity.getOverview());
            movie.setPopularity(movieEntity.getPopularity());
            movie.setPosterPath(movieEntity.getPosterPath());
            movie.setReleaseDate(movieEntity.getReleaseDate());
            movie.setTitle(movieEntity.getTitle());
            movie.setVideo(movieEntity.getVideo());
            movie.setVoteAverage(movieEntity.getVoteAverage());
            movie.setVoteCount(movieEntity.getVoteCount());
            listMovie.add(movie);
        }
        resp.setResults(listMovie);
        return resp;
    }

    public TrailersResp trailersRespToDomain(TrailersRespEntity param){
        TrailersResp trailersResp = new TrailersResp();
        List<Trailer> listTrailers = new ArrayList<>();

        for (int i = 0; i < param.getResults().size(); i++){
            TrailerEntity trailerEntity = param.getResults().get(i);
            Trailer trailer = new Trailer();
            trailer.setId(trailerEntity.getId());
            trailer.setIso6391(trailerEntity.getIso6391());
            trailer.setIso31661(trailerEntity.getIso31661());
            trailer.setKey(trailerEntity.getKey());
            trailer.setName(trailerEntity.getName());
            trailer.setSite(trailerEntity.getSite());
            trailer.setType(trailerEntity.getType());
            trailer.setSize(trailerEntity.getSize());
            listTrailers.add(trailer);
        }

        trailersResp.setId(param.getId());
        trailersResp.setResults(listTrailers);
        return trailersResp;
    }

    public ReviewsResp reviewsRespToDomain(ReviewsRespEntity param){
        ReviewsResp reviewsResp = new ReviewsResp();
        List<Review> listReview = new ArrayList<>();

        for (int i = 0; i < param.getResults().size(); i++){
            ReviewEntity reviewEntity = param.getResults().get(i);
            Review review = new Review();

            review.setAuthor(reviewEntity.getAuthor());
            review.setContent(reviewEntity.getContent());
            review.setId(reviewEntity.getId());
            review.setUrl(reviewEntity.getUrl());
            listReview.add(review);
        }

        reviewsResp.setId(param.getId());
        reviewsResp.setPage(param.getPage());
        reviewsResp.setTotalPages(param.getTotalPages());
        reviewsResp.setTotalResults(param.getTotalResults());
        reviewsResp.setResults(listReview);
        return reviewsResp;
    }
}
