package id.co.bca.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class TrailersRespEntity {
    @SerializedName("id")
    private String id;
    @SerializedName("results")
    private List<TrailerEntity> results;
}
