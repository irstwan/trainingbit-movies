package id.co.bca.data.room;

import android.content.Context;

import androidx.lifecycle.LiveData;

import java.util.List;

import io.reactivex.Completable;

public class RoomManager {

    private FavoriteDao favoriteDao;

    public RoomManager(Context context) {
        AppDatabaseRoom appDatabaseRoom = AppDatabaseRoom.getInstance(context);
        favoriteDao = appDatabaseRoom.favotiteDao();
    }

    public Completable saveFav(FavoriteRoomEntity favoriteRoomEntity){
        return favoriteDao.insertNewFavorite(favoriteRoomEntity);
    }

    public LiveData<List<FavoriteRoomEntity>> getFavoritesMovies(){
        return favoriteDao.loadAllMovieFav();
    }

    public LiveData<FavoriteRoomEntity> getFavoritesMoviesById(String movieId){
        return favoriteDao.getFavoriteMoviesById(movieId);
    }

    public Completable deleteFavoriteMovie(String movieId){
        return favoriteDao.deleteFavoriteMovieById(movieId);
    }
}
