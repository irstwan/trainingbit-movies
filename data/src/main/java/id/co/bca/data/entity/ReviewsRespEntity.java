package id.co.bca.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class ReviewsRespEntity {
    @SerializedName("id")
    private String id;
    @SerializedName("page")
    private String page;
    @SerializedName("total_pages")
    private String totalPages;
    @SerializedName("results")
    private List<ReviewEntity> results;
    @SerializedName("total_results")
    private String totalResults;
}
