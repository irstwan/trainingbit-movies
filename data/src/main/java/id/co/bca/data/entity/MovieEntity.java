package id.co.bca.data.entity;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Data;

@Data
public class MovieEntity {
    @SerializedName("overview")
    private String overview;
    @SerializedName("original_language")
    private String originalLanguage;
    @SerializedName("original_title")
    private String originalTitle;
    @SerializedName("video")
    private String video;
    @SerializedName("title")
    private String title;
    @SerializedName("genre_ids")
    private List<String> genreIds;
    @SerializedName("poster_path")
    private String posterPath;
    @SerializedName("backdrop_path")
    private String backdropPath;
    @SerializedName("release_date")
    private String releaseDate;
    @SerializedName("popularity")
    private String popularity;
    @SerializedName("vote_average")
    private String voteAverage;
    @SerializedName("id")
    private String id;
    @SerializedName("adult")
    private String adult;
    @SerializedName("vote_count")
    private String voteCount;
}
