package id.co.bca.data.repository;

import id.co.bca.data.entity.mapper.MovieMapper;
import id.co.bca.data.net.MoviesService;
import id.co.bca.data.net.ServiceGenerator;
import id.co.bca.domain.model.MoviesResp;
import id.co.bca.domain.model.ReviewsResp;
import id.co.bca.domain.model.TrailersResp;
import id.co.bca.domain.repository.MoviesRepository;
import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;

public class MoviesRepositoryImpl implements MoviesRepository {
    private final MovieMapper movieMapper;
    private final Scheduler backgroundScheduler;

    public MoviesRepositoryImpl() {
        this.movieMapper = new MovieMapper();
        this.backgroundScheduler = Schedulers.io();
    }

    @Override
    public Single<MoviesResp> doGetPopularMovies() {
        MoviesService moviesService = ServiceGenerator.getMoviesService();
        return Single.defer(moviesService::getPopularMovies)
                .map(movieMapper::moviesRespToDomain)
                .subscribeOn(backgroundScheduler);
    }

    @Override
    public Single<MoviesResp> doGetTopRatedMovies() {
        MoviesService moviesService = ServiceGenerator.getMoviesService();
        return Single.defer(moviesService::getTopRatedMovies)
                .map(movieMapper::moviesRespToDomain)
                .subscribeOn(backgroundScheduler);
    }

    @Override
    public Single<TrailersResp> doGetTrailers(String movieId) {
        MoviesService moviesService = ServiceGenerator.getMoviesService();
        return Single.defer(() -> moviesService.getTrailers(movieId)
                .map(movieMapper::trailersRespToDomain)
                .subscribeOn(backgroundScheduler));
    }

    @Override
    public Single<ReviewsResp> doGetReviews(String movieId) {
        MoviesService moviesService = ServiceGenerator.getMoviesService();
        return Single.defer(() -> moviesService.getReviews(movieId)
                .map(movieMapper::reviewsRespToDomain)
                .subscribeOn(backgroundScheduler));
    }
}
