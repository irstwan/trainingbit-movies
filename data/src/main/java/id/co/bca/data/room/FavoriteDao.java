package id.co.bca.data.room;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Completable;
@Dao
public interface FavoriteDao {
    @Insert
    Completable insertNewFavorite(FavoriteRoomEntity favorite);

    @Query("SELECT * FROM tbl_favorite")
    LiveData<List<FavoriteRoomEntity>> loadAllMovieFav();

    @Query("SELECT * FROM tbl_favorite WHERE id=:id")
    LiveData<FavoriteRoomEntity> getFavoriteMoviesById(String id);

    @Query("DELETE FROM tbl_favorite WHERE id = :id")
    Completable deleteFavoriteMovieById(String id);
}
