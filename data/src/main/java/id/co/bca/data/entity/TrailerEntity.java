package id.co.bca.data.entity;

import com.google.gson.annotations.SerializedName;

import lombok.Data;

@Data
public class TrailerEntity {
    @SerializedName("site")
    private String site;
    @SerializedName("size")
    private String size;
    @SerializedName("iso_3166_1")
    private String iso31661;
    @SerializedName("name")
    private String name;
    @SerializedName("id")
    private String id;
    @SerializedName("type")
    private String type;
    @SerializedName("iso_639_1")
    private String iso6391;
    @SerializedName("key")
    private String key;
}
