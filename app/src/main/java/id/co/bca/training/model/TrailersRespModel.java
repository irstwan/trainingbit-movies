package id.co.bca.training.model;

import java.util.List;

import lombok.Data;

@Data
public class TrailersRespModel {
    private String id;
    private List<TrailerModel> results;
}
