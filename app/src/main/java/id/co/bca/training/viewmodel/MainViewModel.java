package id.co.bca.training.viewmodel;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;

import java.util.List;

import javax.inject.Inject;

import id.co.bca.data.room.FavoriteRoomEntity;
import id.co.bca.data.room.RoomManager;
import id.co.bca.domain.usecase.movie.GetPopularMoviesUseCase;
import id.co.bca.domain.usecase.movie.GetTopRatedMoviesUseCase;
import id.co.bca.training.di.DaggerMovieComponent;
import id.co.bca.training.model.FavoriteRoomModel;
import id.co.bca.training.model.MoviesRespModel;
import id.co.bca.training.model.mapper.MoviesMapper;
import id.co.bca.training.viewmodel.repository.MovieRepository;
import io.reactivex.disposables.CompositeDisposable;

public class MainViewModel extends AndroidViewModel implements MoviesInterface.RequiredMovieOps{

    @Inject
    public MoviesMapper movieMapper;
    @Inject
    public GetPopularMoviesUseCase getPopularMoviesUseCase;
    @Inject
    public GetTopRatedMoviesUseCase getTopRatedMoviesUseCase;
    @Inject
    public RoomManager roomManager;

    private MutableLiveData<MoviesRespModel> popularMoviesResp;
    private MutableLiveData<MoviesRespModel> topRatedMoviesResp;
    private MutableLiveData<List<FavoriteRoomModel>> favoriteRoomResp;
    private LiveData<List<FavoriteRoomModel>> favoriteList;
    private MutableLiveData<String> errorMessage;
    private CompositeDisposable mDisposable;
    private Gson mGson = new Gson();
    private MoviesInterface.ProvidedMoviesOps mMovieProvided;

    public MainViewModel(@NonNull Application application) {
        super(application);
        DaggerMovieComponent.create().inject(this);
        mDisposable     = new CompositeDisposable();
        mMovieProvided  = new MovieRepository(this);
    }

    public LiveData<MoviesRespModel> getPopularMovies(){
        if (popularMoviesResp == null){
            popularMoviesResp = new MutableLiveData<>();
            mMovieProvided.getPopularMovies();
        }
        return popularMoviesResp;
    }

    public LiveData<MoviesRespModel> getTopRatedMovies(){
        if (topRatedMoviesResp == null){
            topRatedMoviesResp = new MutableLiveData<>();
            mMovieProvided.getTopRated();
        }
        return topRatedMoviesResp;
    }

    public LiveData<List<FavoriteRoomEntity>> getFavoriteMovies(){
        return roomManager.getFavoritesMovies();
    }

    @Override
    public void onResultPopularMoviesSuccess(MoviesRespModel moviesRespModel) {
        popularMoviesResp.setValue(moviesRespModel);
    }

    @Override
    public void onResultPopularMoviesError(String message) {

    }

    @Override
    public void onResultTopRatedMoviesSuccess(MoviesRespModel moviesRespModel) {
        topRatedMoviesResp.setValue(moviesRespModel);
    }

    @Override
    public void onResultTopRatedMoviesError(String message) {

    }

    @Override
    protected void onCleared() {
        super.onCleared();
        mMovieProvided.onDestroy();
    }
}
