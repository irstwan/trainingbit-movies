package id.co.bca.training.viewmodel;

import id.co.bca.training.model.MoviesRespModel;

public interface MoviesInterface {
    interface RequiredMovieOps{
        void onResultPopularMoviesSuccess(MoviesRespModel moviesRespModel);
        void onResultPopularMoviesError(String message);

        void onResultTopRatedMoviesSuccess(MoviesRespModel moviesRespModel);
        void onResultTopRatedMoviesError(String message);
    }

    interface ProvidedMoviesOps {
        void getPopularMovies();
        void getTopRated();

        void onDestroy();
    }
}
