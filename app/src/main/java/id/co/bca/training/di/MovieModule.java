package id.co.bca.training.di;

import androidx.lifecycle.MutableLiveData;

import dagger.Module;
import dagger.Provides;
import id.co.bca.data.repository.MoviesRepositoryImpl;
import id.co.bca.domain.repository.MoviesRepository;
import id.co.bca.domain.usecase.movie.GetPopularMoviesUseCase;
import id.co.bca.domain.usecase.movie.GetReviewsUseCase;
import id.co.bca.domain.usecase.movie.GetTopRatedMoviesUseCase;
import id.co.bca.domain.usecase.movie.GetTrailersUseCase;
import id.co.bca.training.model.mapper.MoviesMapper;

@Module
public class MovieModule {

    @Provides
    static MoviesMapper provideMovieMapper(){
        return new MoviesMapper();
    }

    @Provides
    static MoviesRepository moviesRepository(){
        return new MoviesRepositoryImpl();
    }

    @Provides
    static GetPopularMoviesUseCase provideGetPopularMoviesUseCase(MoviesRepository moviesRepository){
        return new GetPopularMoviesUseCase(moviesRepository);
    }

    @Provides
    static GetTopRatedMoviesUseCase provideGetTopRatedMoviesUseCase(MoviesRepository moviesRepository){
        return new GetTopRatedMoviesUseCase(moviesRepository);
    }

    @Provides
    static GetTrailersUseCase provideGetTrailersUseCase(MoviesRepository moviesRepository){
        return new GetTrailersUseCase(moviesRepository);
    }

    @Provides
    static GetReviewsUseCase getReviewsUseCase(MoviesRepository moviesRepository) {
        return new GetReviewsUseCase(moviesRepository);
    }
}
