package id.co.bca.training.model;

import java.util.List;

import lombok.Data;

@Data
public class ReviewsRespModel {
    private String id;
    private String page;
    private String totalPages;
    private List<ReviewModel> results;
    private String totalResults;
}
