package id.co.bca.training.presenter;

import javax.inject.Inject;

import id.co.bca.domain.usecase.movie.GetPopularMoviesUseCase;
import id.co.bca.domain.usecase.movie.GetTopRatedMoviesUseCase;
import id.co.bca.training.di.DaggerMovieComponent;
import id.co.bca.training.model.MoviesRespModel;
import id.co.bca.training.model.mapper.MoviesMapper;
import id.co.bca.training.view.MainMVP;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainPresenter implements MainMVP.ProvidedMainPresenterOps{
    private final MainMVP.RequiredMainViewOps mView;
    private final CompositeDisposable mDisposable = new CompositeDisposable();

    @Inject
    public MoviesMapper movieMapper;
    @Inject
    public GetPopularMoviesUseCase getPopularMoviesUseCase;
    @Inject
    public GetTopRatedMoviesUseCase getTopRatedMoviesUseCase;

    public MainPresenter(MainMVP.RequiredMainViewOps mView) {
        this.mView = mView;
        DaggerMovieComponent.create().inject(this);
    }

    @Override
    public void getPopularMovies() {
        mDisposable.add(
                getPopularMoviesUseCase.execute()
                        .map(movieMapper::moviesRespModelToPresentation)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<MoviesRespModel>() {
                            @Override
                            public void onSuccess(MoviesRespModel moviesRespModel) {
                                mView.onResultPopularMoviesSuccess(moviesRespModel);
                            }

                            @Override
                            public void onError(Throwable e) {
                                mView.onResultPopularMoviesError(e.getMessage());
                            }
                        })
        );
    }

    @Override
    public void getTopRated() {
        mDisposable.add(getTopRatedMoviesUseCase.execute()
                .map(movieMapper::moviesRespModelToPresentation)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<MoviesRespModel>() {
                    @Override
                    public void onSuccess(MoviesRespModel moviesRespModel) {
                        mView.onResultTopRatedMoviesSuccess(moviesRespModel);
                    }

                    @Override
                    public void onError(Throwable e) {
                        mView.onResultTopRatedMoviesError(e.getMessage());
                    }
                }));
    }

    @Override
    public void onDestroy() {
        mDisposable.clear();
    }
}
