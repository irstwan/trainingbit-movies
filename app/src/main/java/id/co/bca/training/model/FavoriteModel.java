package id.co.bca.training.model;

import lombok.Data;

@Data
public class FavoriteModel {
    private String id;
    private String realeseDate;
    private String rating;
    private String description;
}
