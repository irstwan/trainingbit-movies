package id.co.bca.training.di;


import javax.inject.Singleton;

import dagger.Component;
import id.co.bca.training.presenter.MainPresenter;
import id.co.bca.training.view.DetailMovieActivity;
import id.co.bca.training.viewmodel.DetailMovieViewModel;
import id.co.bca.training.viewmodel.MainViewModel;
import id.co.bca.training.viewmodel.repository.MovieRepository;

@Singleton
@Component(modules = {MovieModule.class, RoomModule.class})
public interface MovieComponent {
    void inject(MainPresenter mainPresenter);
    void inject(MainViewModel mainViewModel);
    void inject(DetailMovieViewModel detailMovieViewModel);
    void inject(MovieRepository movieRepository);
}
