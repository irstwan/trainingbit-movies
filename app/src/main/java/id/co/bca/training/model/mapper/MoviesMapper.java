package id.co.bca.training.model.mapper;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import id.co.bca.data.room.FavoriteRoomEntity;
import id.co.bca.domain.model.Movie;
import id.co.bca.domain.model.MoviesResp;
import id.co.bca.domain.model.Review;
import id.co.bca.domain.model.ReviewsResp;
import id.co.bca.domain.model.Trailer;
import id.co.bca.domain.model.TrailersResp;
import id.co.bca.training.model.FavoriteModel;
import id.co.bca.training.model.FavoriteRoomModel;
import id.co.bca.training.model.MovieModel;
import id.co.bca.training.model.MoviesRespModel;
import id.co.bca.training.model.ReviewModel;
import id.co.bca.training.model.ReviewsRespModel;
import id.co.bca.training.model.TrailerModel;
import id.co.bca.training.model.TrailersRespModel;

public class MoviesMapper {

    public MoviesRespModel moviesRespModelToPresentation(MoviesResp param){
        MoviesRespModel resp = new MoviesRespModel();
        List<MovieModel> listMovie = new ArrayList<>();
        resp.setPage(param.getPage());
        resp.setTotalPages(param.getTotalPages());
        resp.setTotalResults(param.getTotalResults());

        for (int i = 0; i < param.getResults().size(); i++){
            Movie movie = param.getResults().get(i);
            MovieModel movieModel = new MovieModel();
            movieModel.setAdult(movie.getAdult());
            movieModel.setBackdropPath(movie.getBackdropPath());
            movieModel.setGenreIds(movie.getGenreIds());
            movieModel.setId(movie.getId());
            movieModel.setOriginalLanguage(movie.getOriginalLanguage());
            movieModel.setOriginalTitle(movie.getOriginalTitle());
            movieModel.setOverview(movie.getOverview());
            movieModel.setPopularity(movie.getPopularity());
            movieModel.setPosterPath(movie.getPosterPath());
            movieModel.setReleaseDate(movie.getReleaseDate());
            movieModel.setTitle(movie.getTitle());
            movieModel.setVideo(movie.getVideo());
            movieModel.setVoteAverage(movie.getVoteAverage());
            movieModel.setVoteCount(movie.getVoteCount());
            listMovie.add(movieModel);
        }
        resp.setResults(listMovie);
        return resp;
    }

    public TrailersRespModel trailersRespModelToPresentation(TrailersResp param){
        TrailersRespModel resp = new TrailersRespModel();
        List<TrailerModel> listTrailers = new ArrayList<>();

        for (int i = 0; i < param.getResults().size(); i++){
            Trailer trailer = param.getResults().get(i);
            TrailerModel trailerModel = new TrailerModel();
            trailerModel.setId(trailer.getId());
            trailerModel.setIso6391(trailer.getIso6391());
            trailerModel.setIso31661(trailer.getIso31661());
            trailerModel.setKey(trailer.getKey());
            trailerModel.setName(trailer.getName());
            trailerModel.setSite(trailer.getSite());
            trailerModel.setSize(trailer.getSize());
            trailerModel.setType(trailer.getType());
            listTrailers.add(trailerModel);
        }

        resp.setId(param.getId());
        resp.setResults(listTrailers);
        return resp;
    }

    public ReviewsRespModel reviewsRespModelToPresentation(ReviewsResp param){
        Gson gson = new Gson();
        ReviewsRespModel reviewsRespModel = new ReviewsRespModel();
        List<ReviewModel> listReview = new ArrayList<>();

        for (int i = 0; i < param.getResults().size(); i++){
            Review review = param.getResults().get(i);
            ReviewModel reviewModel = new ReviewModel();
            reviewModel.setAuthor(review.getAuthor());
            reviewModel.setContent(review.getContent());
            reviewModel.setId(review.getId());
            reviewModel.setUrl(review.getUrl());
            listReview.add(reviewModel);
        }
        reviewsRespModel.setId(param.getId());
        reviewsRespModel.setPage(param.getPage());
        reviewsRespModel.setTotalPages(param.getTotalPages());
        reviewsRespModel.setTotalResults(param.getTotalResults());
        reviewsRespModel.setResults(listReview);
        return reviewsRespModel;
    }

    public FavoriteModel favoriteModelToPresentation(FavoriteRoomEntity param){
        FavoriteModel favoriteModel = new FavoriteModel();
        favoriteModel.setDescription(param.getDescription());
        favoriteModel.setId(param.getId());
        favoriteModel.setRealeseDate(param.getRealeseDate());
        favoriteModel.setRating(param.getRating());

        return favoriteModel;
    }

    public List<MovieModel> favoriteRoomToMovieModel(List<FavoriteRoomModel> param){
        List<MovieModel> movieModelList = new ArrayList<>();
        for (int i=0; i < param.size(); i++) {
            FavoriteRoomModel favoriteRoomModel = param.get(i);
            MovieModel movieModel = new MovieModel();
            movieModel.setId(favoriteRoomModel.getId());
            movieModel.setOverview(favoriteRoomModel.getDescription());
            movieModel.setReleaseDate(favoriteRoomModel.getRealeseDate());
            movieModel.setVoteAverage(favoriteRoomModel.getRating());
            movieModel.setPosterPath(favoriteRoomModel.getPosterPath());
            movieModel.setBackdropPath(favoriteRoomModel.getBackdropPath());
            movieModel.setTitle(favoriteRoomModel.getTitle());
            movieModelList.add(movieModel);
        }

        return movieModelList;
    }

    public List<MovieModel> favoriteRoomEntityToPresentation(List<FavoriteRoomEntity> param){
        List<MovieModel> movieModelList = new ArrayList<>();
        for (int i=0; i < param.size(); i++) {
            FavoriteRoomEntity favoriteRoomEntity = param.get(i);
            MovieModel movieModel = new MovieModel();
            movieModel.setId(favoriteRoomEntity.getId());
            movieModel.setOverview(favoriteRoomEntity.getDescription());
            movieModel.setReleaseDate(favoriteRoomEntity.getRealeseDate());
            movieModel.setVoteAverage(favoriteRoomEntity.getRating());
            movieModel.setPosterPath(favoriteRoomEntity.getPosterPath());
            movieModel.setBackdropPath(favoriteRoomEntity.getBackdropPath());
            movieModel.setTitle(favoriteRoomEntity.getTitle());
            movieModelList.add(movieModel);
        }

        return movieModelList;
    }
}
