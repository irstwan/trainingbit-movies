package id.co.bca.training.view;

import id.co.bca.training.model.MoviesRespModel;

public interface MainMVP {
    interface RequiredMainViewOps{
        void onResultPopularMoviesSuccess(MoviesRespModel moviesRespModel);
        void onResultPopularMoviesError(String message);

        void onResultTopRatedMoviesSuccess(MoviesRespModel moviesRespModel);
        void onResultTopRatedMoviesError(String message);
    }

    interface ProvidedMainPresenterOps{
        void getPopularMovies();
        void getTopRated();

        void onDestroy();
    }
}
