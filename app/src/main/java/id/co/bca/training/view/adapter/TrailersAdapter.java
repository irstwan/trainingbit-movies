package id.co.bca.training.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.bca.training.R;
import id.co.bca.training.model.MovieModel;
import id.co.bca.training.model.TrailerModel;
import id.co.bca.training.view.ClickListener;

public class TrailersAdapter extends RecyclerView.Adapter<TrailersAdapter.ViewHolder> {
    private final List<TrailerModel> mTrailerList;
    private final Context context;
    private final String imgUrl = "http://image.tmdb.org/t/p/w342/";
    private ClickListener clickListener;

    public TrailersAdapter(List<TrailerModel> mTrailerList, Context context) {
        this.mTrailerList = mTrailerList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_trailers, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final TrailerModel item = mTrailerList.get(position);
        String thumbnailUrl = "http://img.youtube.com/vi/" + item.getKey() + "/0.jpg";

        Picasso.get()
                .load(thumbnailUrl)
                .config(Bitmap.Config.RGB_565)
                .placeholder(R.drawable.image_placeholder)
                .into(holder.mTrailerThumbnail);

        holder.mTrailerThumbnail.setOnClickListener(v -> clickListener.onClick(v, position));
    }


    @Override
    public int getItemCount() {
        return mTrailerList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.trailer_thumbnail) ImageView mTrailerThumbnail;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setClickListener(ClickListener _clickListener) {
        this.clickListener = _clickListener;
    }
}
