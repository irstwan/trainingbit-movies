package id.co.bca.training.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import id.co.bca.data.room.FavoriteRoomEntity;
import id.co.bca.data.room.RoomManager;
import id.co.bca.domain.usecase.movie.GetReviewsUseCase;
import id.co.bca.domain.usecase.movie.GetTrailersUseCase;
import id.co.bca.training.di.DaggerMovieComponent;
import id.co.bca.training.model.MovieModel;
import id.co.bca.training.model.ReviewsRespModel;
import id.co.bca.training.model.TrailersRespModel;
import id.co.bca.training.model.mapper.MoviesMapper;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class DetailMovieViewModel  extends AndroidViewModel {

    private CompositeDisposable mDisposable;
    private MutableLiveData<TrailersRespModel> trailersResp;
    private MutableLiveData<ReviewsRespModel> reviewResp;

    @Inject
    public MoviesMapper movieMapper;
    @Inject
    public GetTrailersUseCase getTrailersUseCase;
    @Inject
    public GetReviewsUseCase getReviewsUseCase;
    @Inject
    public RoomManager roomManager;

    public DetailMovieViewModel(@NonNull Application application) {
        super(application);
        DaggerMovieComponent.create().inject(this);
        mDisposable = new CompositeDisposable();
    }

    public LiveData<TrailersRespModel> getTrailers(String movieId){
        if (trailersResp == null){
            trailersResp = new MutableLiveData<>();
            loadTrailers(movieId);
        }
        return trailersResp;
    }

    public LiveData<ReviewsRespModel> getReviews(String movieId){
        if (reviewResp == null){
            reviewResp = new MutableLiveData<>();
            loadReviews(movieId);
        }
        return reviewResp;
    }

    public void loadTrailers(String movieId){
        mDisposable.add(
                getTrailersUseCase.execute(movieId)
                        .map(movieMapper::trailersRespModelToPresentation)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<TrailersRespModel>() {
                            @Override
                            public void onSuccess(TrailersRespModel trailersRespModel) {
                                trailersResp.setValue(trailersRespModel);
                            }

                            @Override
                            public void onError(Throwable e) {

                            }
                        })
        );
    }

    public void loadReviews(String movieId){
        mDisposable.add(
                getReviewsUseCase.execute(movieId)
                        .map(movieMapper::reviewsRespModelToPresentation)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribeWith(new DisposableSingleObserver<ReviewsRespModel>() {
                            @Override
                            public void onSuccess(ReviewsRespModel reviewsRespModel) {
                                reviewResp.setValue(reviewsRespModel);
                            }

                            @Override
                            public void onError(Throwable e) {

                            }
                        })
        );
    }

    public LiveData<FavoriteRoomEntity> getFavoriteMoviesById(String movieId){
        return roomManager.getFavoritesMoviesById(movieId);
    }

    public void saveFavoriteMovie(MovieModel movieModel){
        FavoriteRoomEntity favoriteRoomEntity = new FavoriteRoomEntity();
        favoriteRoomEntity.setDescription(movieModel.getOverview());
        favoriteRoomEntity.setId(movieModel.getId());
        favoriteRoomEntity.setRating(movieModel.getVoteAverage());
        favoriteRoomEntity.setRealeseDate(movieModel.getReleaseDate());
        favoriteRoomEntity.setPosterPath(movieModel.getPosterPath());
        favoriteRoomEntity.setBackdropPath(movieModel.getBackdropPath());
        favoriteRoomEntity.setTitle(movieModel.getTitle());
        roomManager.saveFav(favoriteRoomEntity)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onError(Throwable throwable) {

                    }
                });
    }

    public void deleteFavoriteMovie(String idMovie){
        roomManager.deleteFavoriteMovie(idMovie)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }

//    @Override
//    protected void onCleared() {
//        super.onCleared();
//        if (mDisposable != null) {
//            mDisposable.clear();
//            mDisposable = null;
//        }
//    }
}
