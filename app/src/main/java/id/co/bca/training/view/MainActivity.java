package id.co.bca.training.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;

import com.google.gson.Gson;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.bca.data.room.RoomManager;
import id.co.bca.training.R;
import id.co.bca.training.di.DaggerRoomComponent;
import id.co.bca.training.model.MovieModel;
import id.co.bca.training.model.mapper.MoviesMapper;
import id.co.bca.training.view.adapter.MoviesAdapter;
import id.co.bca.training.viewmodel.MainViewModel;

public class MainActivity extends AppCompatActivity{

    private MoviesMapper moviesMapper = new MoviesMapper();
    private Gson mGson = new Gson();
    private MainViewModel viewModel;
    private static final String POPULAR = "POPULAR";
    private static final String TOP_RATED = "TOP_RATED";
    private static final String FAVORITE = "FAVORITE";
    private static final String EXTRA_SORT_BY = "EXTRA_SORT_BY";
    private static final String EXTRA_TITLE = "EXTRA_TITLE";
    private String mTitle = "Popular Movies";
    private String mSortBy = POPULAR;
    @BindView(R.id.indeterminateBar) ProgressBar mProgressBar;
    @BindView(R.id.recycled_movie_grid) RecyclerView mMovieRecyclerView;
    @Inject RoomManager roomManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        DaggerRoomComponent.create().inject(this);

        mProgressBar.setVisibility(View.INVISIBLE);
        viewModel = ViewModelProviders.of(this).get(MainViewModel.class);
        if (savedInstanceState == null){
            chooseMenu(mSortBy);
            setTitle(mTitle);
        } else {
            mSortBy = savedInstanceState.getString(EXTRA_SORT_BY);
            mTitle = savedInstanceState.getString(EXTRA_TITLE);
            setTitle(mTitle);
            chooseMenu(mSortBy);
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(EXTRA_SORT_BY, mSortBy);
        outState.putString(EXTRA_TITLE, mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;

    }

    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId()==R.id.popular){
            mTitle = "Popular Movies";
            setTitle(mTitle);
            mSortBy = POPULAR;
            chooseMenu(mSortBy);
        } else if (item.getItemId() == R.id.top_rated) {
            mTitle = "Top Rated Movies";
            setTitle(mTitle);
            mSortBy = TOP_RATED;
            chooseMenu(mSortBy);
        } else if (item.getItemId() == R.id.favorite) {
            mTitle = "Favorite";
            setTitle(mTitle);
            mSortBy = FAVORITE;
            chooseMenu(mSortBy);
        }

        return true;
    }

    public void chooseMenu(String menu){
        switch (menu){
            case POPULAR:
                getPopularMovies();
                break;
            case TOP_RATED:
                getTopRatedMovies();
                break;
            case FAVORITE:
                getFavoritesMovies();
                break;
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void getPopularMovies(){
        viewModel.getPopularMovies().observe(this, moviesRespModel -> setUp(moviesRespModel.getResults()));
    }

    private void getTopRatedMovies(){
        viewModel.getTopRatedMovies().observe(this, moviesRespModel -> setUp(moviesRespModel.getResults()));
    }

    private void getFavoritesMovies() {
        viewModel.getFavoriteMovies().observe(this, favoriteRoomEntities -> {
            setUp(moviesMapper.favoriteRoomEntityToPresentation(favoriteRoomEntities));
            setTitle("Favorite");
        });
    }

    private void setUp(List<MovieModel> listMovie){
        MoviesAdapter mAdapter = new MoviesAdapter(listMovie, getApplicationContext());
        RecyclerView.LayoutManager manager = new GridLayoutManager(this,2);
        mMovieRecyclerView.setLayoutManager(manager);
        mMovieRecyclerView.setAdapter(mAdapter);

        mAdapter.setClickListener(new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                MovieModel movieModel = listMovie.get(position);

                Intent intent = new Intent(MainActivity.this, DetailMovieActivity.class);
                intent.putExtra(DetailMovieActivity.jsonMovieDetail, mGson.toJson(movieModel));
                startActivity(intent);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });
    }


}
