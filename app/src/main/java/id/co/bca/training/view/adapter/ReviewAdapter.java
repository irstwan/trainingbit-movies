package id.co.bca.training.view.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import id.co.bca.training.R;
import id.co.bca.training.model.ReviewModel;
import id.co.bca.training.view.ClickListener;

public class ReviewAdapter extends RecyclerView.Adapter<ReviewAdapter.ViewHolder> {
    private final List<ReviewModel> mReviewsList;
    private final Context context;
    private ClickListener clickListener;

    public ReviewAdapter(List<ReviewModel> mReviewsList, Context context) {
        this.mReviewsList = mReviewsList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reviews, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final ReviewModel item = mReviewsList.get(position);
        holder.mAuthor.setText(item.getAuthor());
        holder.mContent.setText(item.getContent());

        holder.mContent.setOnClickListener(v -> clickListener.onClick(v, position));
    }


    @Override
    public int getItemCount() {
        return mReviewsList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.review_author) TextView mAuthor;
        @BindView(R.id.review_content) TextView mContent;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public void setClickListener(ClickListener _clickListener) {
        this.clickListener = _clickListener;
    }
}
