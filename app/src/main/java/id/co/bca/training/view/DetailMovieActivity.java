package id.co.bca.training.view;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import id.co.bca.data.room.FavoriteRoomEntity;
import id.co.bca.data.room.RoomManager;
import id.co.bca.training.R;
import id.co.bca.training.di.DaggerRoomComponent;
import id.co.bca.training.model.MovieModel;
import id.co.bca.training.model.ReviewModel;
import id.co.bca.training.model.ReviewsRespModel;
import id.co.bca.training.model.TrailerModel;
import id.co.bca.training.model.TrailersRespModel;
import id.co.bca.training.view.adapter.ReviewAdapter;
import id.co.bca.training.view.adapter.TrailersAdapter;
import id.co.bca.training.viewmodel.DetailMovieViewModel;

public class DetailMovieActivity extends AppCompatActivity {

    @BindView(R.id.tv_title) TextView mTitle;
    @BindView(R.id.tv_val_description) TextView mDescription;
    @BindView(R.id.tv_val_realese_date) TextView mReleaseDate;
    @BindView(R.id.img_movie) ImageView imgMovie;
    @BindView(R.id.detail_toolbar) Toolbar mToolbar;
    @BindView(R.id.movie_backdrop) ImageView movieBackrop;
    @BindView(R.id.rv_trailers) RecyclerView rvTrailers;
    @BindView(R.id.rv_reviews) RecyclerView rvReviews;
    @BindView(R.id.backdrop_background) ImageView backgound;
    @BindViews({R.id.rating_first_star, R.id.rating_second_star, R.id.rating_third_star, R.id.rating_fourth_star, R.id.rating_fifth_star})
    List<ImageView> ratingStarViews;

    @Inject
    public RoomManager roomManager;

    public static final String jsonMovieDetail = "JSON_MOVIE";
    public static final String KEY_TRAILER = null;
    private Gson mGson = new Gson();
    private DetailMovieViewModel mDetailMovieViewModel;
    private List<TrailerModel> listTrailers;
    private List<ReviewModel> listReviews;
    private MovieModel movieModel;
    private FavoriteRoomEntity favoriteRoomEntity;

    private final String imgUrl = "http://image.tmdb.org/t/p/w342";
    private final String trailerUrl = "http://www.youtube.com/watch?v=";
    private Menu mGlobalMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_movie);
        ButterKnife.bind(this);
//        setSupportActionBar(mToolbar);
//        ActionBar actionBar = getSupportActionBar();
//        if (actionBar != null) {
//            actionBar.setDisplayHomeAsUpEnabled(true);
//        }

        mDetailMovieViewModel = ViewModelProviders.of(this).get(DetailMovieViewModel.class);

        DaggerRoomComponent.create().inject(this);
        Intent intent           = getIntent();
        String jsonMovieIntent  = intent.getStringExtra(jsonMovieDetail);
        movieModel   = mGson.fromJson(jsonMovieIntent, MovieModel.class);
        getFavoriteMovieById(movieModel.getId());
        mTitle.setText(movieModel.getTitle());
        setRating();
        mDescription.setText(movieModel.getOverview());
        mReleaseDate.setText(movieModel.getReleaseDate());

        Picasso.get()
                .load(imgUrl+movieModel.getPosterPath())
                .config(Bitmap.Config.RGB_565)
                .placeholder(R.drawable.image_placeholder)
                .into(imgMovie);

        Picasso.get()
                .load(imgUrl+movieModel.getBackdropPath())
                .config(Bitmap.Config.RGB_565)
                .placeholder(R.drawable.image_placeholder)
                .into(movieBackrop);

        getReviews(movieModel.getId());
        getTrailers(movieModel.getId());
        setTitle(movieModel.getTitle());
        backgound.bringToFront();
        setUpToolBar();
    }

    public void getTrailers(String movieId){
        mDetailMovieViewModel.getTrailers(movieId).observe(this, trailersRespModel -> setUpTrailers(trailersRespModel));
    }

    public void getReviews(String movieId){
        mDetailMovieViewModel.getReviews(movieId).observe(this, reviewsRespModel -> setUpReview(reviewsRespModel));
    }

    public void getFavoriteMovieById(String movieId){
        mDetailMovieViewModel.getFavoriteMoviesById(movieId).observe(this, resp -> {
            favoriteRoomEntity = null;
            if (resp != null) {
                favoriteRoomEntity = resp;
            }
            updateOptionMenu();
        });
    }

    public void setUpTrailers(TrailersRespModel trailersRespModel){
        listTrailers = trailersRespModel.getResults();
        TrailersAdapter mAdapter = new TrailersAdapter(listTrailers, getApplicationContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        rvTrailers.setLayoutManager(layoutManager);
        rvTrailers.setAdapter(mAdapter);
        rvTrailers.setNestedScrollingEnabled(false);

        mAdapter.setClickListener(new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                TrailerModel trailerModel = trailersRespModel.getResults().get(position);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(trailerUrl+trailerModel.getKey())));
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });
    }

    public void setUpReview(ReviewsRespModel reviewsRespModel){
        listReviews = reviewsRespModel.getResults();
        ReviewAdapter mAdapter = new ReviewAdapter(listReviews, getApplicationContext());
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
        rvReviews.setLayoutManager(layoutManager);
        rvReviews.setAdapter(mAdapter);

        mAdapter.setClickListener(new ClickListener() {
            @Override
            public void onClick(View view, int position) {
                ReviewModel reviewModel = listReviews.get(position);
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(reviewModel.getUrl())));
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishAfterTransition();
    }

    protected Toolbar setUpToolBar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        } else {
        }
        if (mToolbar != null) {
            mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }
        return mToolbar;
    }

    private void setRating() {
        if (movieModel.getVoteAverage() != null && !movieModel.getVoteAverage().isEmpty()) {
            float user_rating = Float.valueOf(movieModel.getVoteAverage()) / 2;
            int integerPart = (int) user_rating;
            // Fill stars
            for (int i = 0; i < integerPart; i++) {
                ratingStarViews.get(i).setImageResource(R.drawable.ic_star_red_24dp);
            }
            // Fill half star
            if (Math.round(user_rating) > integerPart) {
                ratingStarViews.get(integerPart).setImageResource(
                        R.drawable.ic_star_half_red_24dp);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_favorite, menu);
        mGlobalMenu = menu;
        return true;
    }

    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem menuItem = menu.findItem(R.id.action_favorite);
        if (favoriteRoomEntity == null){
            menuItem.setIcon(R.drawable.ic_favorite_border);
        } else {
            menuItem.setIcon(R.drawable.ic_favorite);
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        getFavoriteMovieById(movieModel.getId());

        if (favoriteRoomEntity == null){
            item.setIcon(R.drawable.ic_favorite_border);
            mDetailMovieViewModel.saveFavoriteMovie(movieModel);
        } else {
            item.setIcon(R.drawable.ic_favorite);
            mDetailMovieViewModel.deleteFavoriteMovie(movieModel.getId());
        }

        return true;
    }

    public void updateOptionMenu(){
        if (mGlobalMenu != null){
            onPrepareOptionsMenu(mGlobalMenu);
        }
    }

}
