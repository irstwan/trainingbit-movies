package id.co.bca.training.view.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import id.co.bca.training.R;
import id.co.bca.training.model.MovieModel;
import id.co.bca.training.view.ClickListener;

public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.ViewHolder> {
    private final List<MovieModel> mRMoviesList;
    private final Context context;
    private final String imgUrl = "http://image.tmdb.org/t/p/w342/";
    private ClickListener clickListener;

    public MoviesAdapter(List<MovieModel> mRMoviesList, Context context) {
        this.mRMoviesList = mRMoviesList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        final MovieModel item = mRMoviesList.get(position);

        String posterUrl = imgUrl+item.getPosterPath();
        if (item.getPosterPath() == null) {
            holder.mMovietitle.setVisibility(View.VISIBLE);
        }

        Picasso.get()
                .load(posterUrl)
                .config(Bitmap.Config.RGB_565)
                .placeholder(R.drawable.image_placeholder)
                .into(holder.mMovieThumbnail
                );

        holder.mMovietitle.setText(item.getTitle());


        holder.mMovieThumbnail.setOnClickListener(v -> clickListener.onClick(v, position));
        holder.setRating(item.getVoteAverage());
    }


    @Override
    public int getItemCount() {
        return mRMoviesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.movie_thumbnail)
        ImageView mMovieThumbnail;

        @BindView(R.id.movie_title)
        TextView mMovietitle;
        @BindViews({R.id.rating_first_star, R.id.rating_second_star, R.id.rating_third_star, R.id.rating_fourth_star, R.id.rating_fifth_star})
        List<ImageView> ratingStarViews;

        ViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }

        private void setRating(String rating) {
            if (rating != null && !rating.isEmpty()) {
                float user_rating = Float.valueOf(rating) / 2;
                int integerPart = (int) user_rating;
                // Fill stars
                for (int i = 0; i < integerPart; i++) {
                    ratingStarViews.get(i).setImageResource(R.drawable.ic_star_red_24dp);
                }
                // Fill half star
                if (Math.round(user_rating) > integerPart) {
                    ratingStarViews.get(integerPart).setImageResource(
                            R.drawable.ic_star_half_red_24dp);
                }
            }
        }
    }

    public void setClickListener(ClickListener _clickListener) {
        this.clickListener = _clickListener;
    }
}
