package id.co.bca.training.model;

import lombok.Data;

@Data
public class TrailerModel {
    private String site;
    private String size;
    private String iso31661;
    private String name;
    private String id;
    private String type;
    private String iso6391;
    private String key;
}
