package id.co.bca.training.model;

import java.util.List;

import lombok.Data;

@Data
public class MoviesRespModel {
    private int page;
    private int totalResults;
    private int totalPages;
    private List<MovieModel> results;
}
