package id.co.bca.training.model;

import lombok.Data;

@Data
public class ReviewModel {
    private String author;
    private String id;
    private String content;
    private String url;
}
