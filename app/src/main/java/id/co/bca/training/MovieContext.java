package id.co.bca.training;

import android.app.Application;
import android.content.Context;

public class MovieContext extends Application {
    private static Context context;

    public void onCreate() {
        super.onCreate();
        MovieContext.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MovieContext.context;
    }
}
