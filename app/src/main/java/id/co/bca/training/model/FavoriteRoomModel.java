package id.co.bca.training.model;

import lombok.Data;


@Data
public class FavoriteRoomModel {
    private String id;
    private String realeseDate;
    private String rating;
    private String description;
    private String posterPath;
    private String backdropPath;
    private String title;
}
