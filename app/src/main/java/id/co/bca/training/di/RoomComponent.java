package id.co.bca.training.di;

import javax.inject.Singleton;

import dagger.Component;
import id.co.bca.training.view.DetailMovieActivity;
import id.co.bca.training.view.MainActivity;

@Singleton
@Component(modules = RoomModule.class)
public interface RoomComponent {
    void inject(DetailMovieActivity detailMovieActivity);
    void inject(MainActivity mainActivity);
}
