package id.co.bca.training.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import id.co.bca.data.room.RoomManager;
import id.co.bca.training.MovieContext;

@Module
public class RoomModule {
    @Provides
    @Singleton
    MovieContext provideMovieContext(){
        return new MovieContext();
    }

    @Provides
    @Singleton
    RoomManager provideRoomManager(){
        return new RoomManager(MovieContext.getAppContext());
    }
}
