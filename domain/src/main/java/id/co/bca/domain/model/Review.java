package id.co.bca.domain.model;

import lombok.Data;

@Data
public class Review {
    private String author;
    private String id;
    private String content;
    private String url;
}
