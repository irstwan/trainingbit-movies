package id.co.bca.domain.repository;

import id.co.bca.domain.model.MoviesResp;
import id.co.bca.domain.model.ReviewsResp;
import id.co.bca.domain.model.TrailersResp;
import io.reactivex.Single;

public interface MoviesRepository {
    Single<MoviesResp> doGetPopularMovies();
    Single<MoviesResp> doGetTopRatedMovies();
    Single<TrailersResp> doGetTrailers(String movieId);
    Single<ReviewsResp> doGetReviews(String movieId);
}
