package id.co.bca.domain.model;

import java.util.List;

import lombok.Data;

@Data
public class ReviewsResp {
    private String id;
    private String page;
    private String totalPages;
    private List<Review> results;
    private String totalResults;
}
