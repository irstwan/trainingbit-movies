package id.co.bca.domain.usecase.movie;

import id.co.bca.domain.model.ReviewsResp;
import id.co.bca.domain.model.TrailersResp;
import id.co.bca.domain.repository.MoviesRepository;
import id.co.bca.domain.usecase.SingleUseCaseWithParameter;
import io.reactivex.Single;

public class GetReviewsUseCase implements SingleUseCaseWithParameter<String, ReviewsResp> {
    private final MoviesRepository moviesRepository;

    public GetReviewsUseCase(MoviesRepository moviesRepository) {
        this.moviesRepository = moviesRepository;
    }

    @Override
    public Single<ReviewsResp> execute(String movieId) {
        return moviesRepository.doGetReviews(movieId);
    }
}
