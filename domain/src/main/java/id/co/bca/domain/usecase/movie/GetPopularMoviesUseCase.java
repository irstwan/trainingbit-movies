package id.co.bca.domain.usecase.movie;

import id.co.bca.domain.model.MoviesResp;
import id.co.bca.domain.repository.MoviesRepository;
import id.co.bca.domain.usecase.SingleUseCase;
import io.reactivex.Single;

public class GetPopularMoviesUseCase implements SingleUseCase<MoviesResp> {
    private final MoviesRepository moviesRepository;

    public GetPopularMoviesUseCase(MoviesRepository moviesRepository) {
        this.moviesRepository = moviesRepository;
    }

    @Override
    public Single<MoviesResp> execute() {
        return Single.defer(moviesRepository::doGetPopularMovies);
    }
}
