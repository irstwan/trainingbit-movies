package id.co.bca.domain.model;

import java.util.List;

import lombok.Data;

@Data
public class MoviesResp {
    private int page;
    private int totalResults;
    private int totalPages;
    private List<Movie> results;
}
