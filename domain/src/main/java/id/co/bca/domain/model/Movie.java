package id.co.bca.domain.model;

import java.util.List;

import lombok.Data;

@Data
public class Movie {
    private String overview;
    private String originalLanguage;
    private String originalTitle;
    private String video;
    private String title;
    private List<String> genreIds;
    private String posterPath;
    private String backdropPath;
    private String releaseDate;
    private String popularity;
    private String voteAverage;
    private String id;
    private String adult;
    private String voteCount;
}
