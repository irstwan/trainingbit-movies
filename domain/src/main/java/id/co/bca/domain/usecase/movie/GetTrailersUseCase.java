package id.co.bca.domain.usecase.movie;

import id.co.bca.domain.model.MoviesResp;
import id.co.bca.domain.model.TrailersResp;
import id.co.bca.domain.repository.MoviesRepository;
import id.co.bca.domain.usecase.SingleUseCase;
import id.co.bca.domain.usecase.SingleUseCaseWithParameter;
import io.reactivex.Single;

public class GetTrailersUseCase implements SingleUseCaseWithParameter<String, TrailersResp> {
    private final MoviesRepository moviesRepository;

    public GetTrailersUseCase(MoviesRepository moviesRepository) {
        this.moviesRepository = moviesRepository;
    }

    @Override
    public Single<TrailersResp> execute(String movieId) {
        return moviesRepository.doGetTrailers(movieId);
    }
}
