package id.co.bca.domain.model;

import java.util.List;

import lombok.Data;

@Data
public class TrailersResp {
    private String id;
    private List<Trailer> results;
}
